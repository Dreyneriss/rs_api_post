const express = require('express')
const postController = require('./controller')
const router = express.Router()

router.get('/post/getPosts', postController.getPosts)
router.get('/post/getPosts/post/:id', postController.getPost)
router.get('/post/getPosts/user/:userId', postController.getPostsForUser)
router.post('/post/addPost', postController.createPost)
router.put('/post/editPost', postController.editPost)
//router.patch('/posts', postController.updateComment)
router.delete('/post/deletePost/post/:id',postController.removePost)
router.delete('/post/deletePost/user/:id',postController.removePostsFromUser)
router.get('/post/getPostCount',postController.countPosts)
router.get('/post/getPostCount/user/:userId',postController.countPostsForUser)

module.exports = router
