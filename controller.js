const postController = {}

const Pool = require("pg").Pool;
const pool = new Pool({
    user: 'picarrotAdmin',
    host: '51.159.24.37',
    database: 'db_picarrot_post',
    password: 'e}KY8x>1Ad?sC_r:BG@n3',
    port: 36144,
})

postController.getPosts = async (req,res)=> {
    pool.query('SELECT * FROM post', (error, results) => {
        if (error) {
            throw error
        }
        res.status(200).json(results.rows)
    })
}
postController.getPost = async (req,res)=> {
    const id = req.params.id.toString()
    pool.query('SELECT * FROM post WHERE uuid_post = $1', [id], (error, results) => {
        if (error) {
            throw error
        }
        res.status(200).json(results.rows)
   })
}
postController.getPostsForUser = async (req,res)=> {
    const userId = req.params.userId.toString()
    pool.query('SELECT * FROM post WHERE uuid_user = $1', [userId], (error, results) => {
        if (error) {
            throw error
        }
        res.status(200).json(results.rows)
    })
}
// remarque :
// J'ai mis uid_post et uid_user dans les paramètres d'adresse car, vu que c'était des ints, ils faisaient chier.
//  J'y comprends pas grand chose, franchement...
// Mais du coup, étant donné qu'à terme les uids seront des... uuid ! Eh bien ce seront des strings et
//  ça passera mieux par le body que par l'adresse. Enfin, ça n'est que mon avis.
postController.createPost = async (req,res)=> {
    const { user, content, imageId } = req.body
    
    let date_ob = new Date();
    let day = ("0" + date_ob.getDate()).slice(-2);
    let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
    let year = date_ob.getFullYear();

    let hours = date_ob.getHours();
    let minutes = date_ob.getMinutes();
    let seconds = date_ob.getSeconds();
    const date = year + "-" + month + "-" + day + " " + hours+":"+minutes+":"+seconds;

    pool.query('INSERT INTO post(uuid_user, post_content, post_imagename, post_date) VALUES ($1, $2, $3, $4)', [user, content, imageId, date], (error, results) => {
        if (error) {
            throw error
        }
        res.status(201).send(`post added`)
    })
}
// Que pourrait-on vouloir modifier ?
postController.editPost = async (req,res)=> {
    const { id, content } = req.body

    let date_ob = new Date();
    let day = ("0" + date_ob.getDate()).slice(-2);
    let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
    let year = date_ob.getFullYear();

    let hours = date_ob.getHours();
    let minutes = date_ob.getMinutes();
    let seconds = date_ob.getSeconds();
    const newDate = year + "-" + month + "-" + day + " " + hours+":"+minutes+":"+seconds;

    pool.query(
        'UPDATE post SET post_content = $1, post_edit_date = $2 WHERE uuid_post = $3',
        [content, newDate, id],
        (error, results) => {
            if (error) {
                throw error
            }
            res.status(200).send(`Comment modified with ID: ${id}`)
        }
    )
}
postController.updateLikes = async (req,res)=> {
    const { idPost } = req.body
    // gotta count the number of likes through the like api

    /*pool.query(
        'UPDATE Post SET post_nb_like = Count(*) WHERE uuid_post = $4 GROUP BY uuid_post',
        [comment, newDate, newTime, id],
        (error, results) => {
            if (error) {
                throw error
            }
            res.status(200).send(`Comment modified with ID: ${id}`)
        }
    )*/
}
postController.removePost = async (req,res)=> {
    const id = req.params.id.toString()

    pool.query('DELETE FROM post WHERE uuid_post = $1', [id], (error, results) => {
        if (error) {
            throw error
        }
        res.status(200).send(`Post deleted with ID: ${id}`)
    })
}
postController.removePostsFromUser = async (req,res)=> {
    const id = req.params.id.toString()

    pool.query('DELETE FROM post WHERE uuid_user = $1', [id], (error, results) => {
        if (error) {
            throw error
        }
        res.status(200).send(`Post deleted with user ID: ${id}`)
    })
}
// add optional param : User and Post
postController.countPosts = async (req,res)=> {
    pool.query('SELECT COUNT(*) FROM post', (error, results) => {
        if (error) {
            throw error
        }
        res.status(200).json(results.rows)
    })
}
postController.countPostsForUser = async (req,res)=> {
    const userId = req.params.userId.toString()
    pool.query('SELECT COUNT(*) FROM post WHERE uuid_user = $1', [userId], (error, results) => {
        if (error) {
            throw error
        }
        res.status(200).json(results.rows)
    })
}

module.exports = postController
